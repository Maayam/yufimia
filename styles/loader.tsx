const loaderStyle = `
.loading,.screen.current {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex
  }
  
  .loading {
    flex-flow: column;
    align-items: center;
    justify-content: center;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #fafafa;
    z-index: 100
  }
  
  .loading .loading-logo {
    -webkit-animation: breathe 4.25s ease-in-out infinite;
    animation: breathe 4.25s ease-in-out infinite;
    width: 40%;
    max-width: 550px
  }
  
  @media (max-width: 480px) {
    .loading .loading-logo {
        width:92%
    }
  }
  
  .loading .loading-logo svg {
    width: 100%;
    stroke-dasharray: 1229;
    stroke-dashoffset: 1229;
    -webkit-animation-delay: .85s;
    animation-delay: .85s;
    -webkit-animation-duration: 1.3s;
    animation-duration: 1.3s;
    -webkit-animation-name: logo-appear;
    animation-name: logo-appear;
    -webkit-animation-fill-mode: forwards;
    animation-fill-mode: forwards
  }
  
  .loading .loading-logo svg #leftWing {
    transform-origin: center center;
    transform: scale(.85) rotate(-25deg) translateX(-30px);
    -webkit-animation-delay: .7s;
    animation-delay: .7s;
    -webkit-animation-fill-mode: forwards;
    animation-fill-mode: forwards
  }
  
  .loading .loading-logo svg #leftWing,.loading .loading-logo svg #rightWing {
    opacity: 0;
    -webkit-animation-duration: .45s;
    animation-duration: .45s;
    -webkit-animation-name: wing-appear;
    animation-name: wing-appear
  }
  
  .loading .loading-logo svg #rightWing {
    transform-origin: center center;
    transform: scale(.85) rotate(25deg) translateX(30px);
    -webkit-animation-delay: .8s;
    animation-delay: .8s;
    -webkit-animation-fill-mode: forwards;
    animation-fill-mode: forwards
  }
  
  .loading .dots {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    bottom: 30px
  }
  
  .loading .dots .dot {
    width: 15px;
    height: 15px;
    border-radius: 15px;
    background-image: linear-gradient(45deg,rgba(206,94,166,.701961),#ffbcdc);
    margin: 10px;
    -webkit-animation-name: bounce;
    animation-name: bounce;
    -webkit-animation: bounce .45s ease infinite alternate;
    animation: bounce .45s ease infinite alternate
  }
  
  .loading .dots .dot:nth-child(4) {
    -webkit-animation-delay: .05s;
    animation-delay: .05s
  }
  
  .loading .dots .dot:nth-child(3) {
    -webkit-animation-delay: .13s;
    animation-delay: .13s
  }
  
  .loading .dots .dot:nth-child(2) {
    -webkit-animation-delay: .2s;
    animation-delay: .2s
  }
  
  .loading .dots .dot:first-child {
    -webkit-animation-delay: .27s;
    animation-delay: .27s
  }
  
  @-webkit-keyframes breathe {
    0% {
        transform: translateY(0)
    }
  
    3% {
        transform: translateY(0)
    }
  
    47% {
        transform: translateY(-25px)
    }
  
    53% {
        transform: translateY(-25px)
    }
  
    97% {
        transform: translateY(0)
    }
  
    to {
        transform: translateY(0)
    }
  }
  
  @keyframes breathe {
    0% {
        transform: translateY(0)
    }
  
    3% {
        transform: translateY(0)
    }
  
    47% {
        transform: translateY(-25px)
    }
  
    53% {
        transform: translateY(-25px)
    }
  
    97% {
        transform: translateY(0)
    }
  
    to {
        transform: translateY(0)
    }
  }
  
  @-webkit-keyframes bounce {
    0% {
        transform: translateY(0);
        height: 7px
    }
  
    20% {
        height: 15px
    }
  
    to {
        transform: translateY(-50px);
        height: 15px
    }
  }
  
  @keyframes bounce {
    0% {
        transform: translateY(0);
        height: 7px
    }
  
    20% {
        height: 15px
    }
  
    to {
        transform: translateY(-50px);
        height: 15px
    }
  }
  
  @-webkit-keyframes wing-appear {
    to {
        opacity: 1;
        transform: scale(1) rotate(0deg) translateX(0)
    }
  }
  
  @keyframes wing-appear {
    to {
        opacity: 1;
        transform: scale(1) rotate(0deg) translateX(0)
    }
  }
  
  @-webkit-keyframes logo-appear {
    0% {
        stroke-dashoffset: 1229
    }
  
    to {
        stroke-dashoffset: 2458
    }
  }
  
  @keyframes logo-appear {
    0% {
        stroke-dashoffset: 1229
    }
  
    to {
        stroke-dashoffset: 2458
    }
  }
  
  @-webkit-keyframes rotation {
    0% {
        transform: rotate(0deg)
    }
  
    50% {
        transform: rotate(180deg)
    }
  
    to {
        transform: rotate(1turn)
    }
  }
  
  @keyframes rotation {
    0% {
        transform: rotate(0deg)
    }
  
    50% {
        transform: rotate(180deg)
    }
  
    to {
        transform: rotate(1turn)
    }
  }
  `;

export default loaderStyle;
