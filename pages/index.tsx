import type { NextPage } from 'next';
import { HomeBanner } from '../components/HomeBanner';
import React from 'react';

const Home: NextPage = () => {
  return <HomeBanner />;
};


export default Home;
