import type { NextPage } from 'next';
import Image, { StaticImageData } from 'next/image';
import img1 from '../public/gallery/Maximizer-p.png';
import img2 from '../public/gallery/Maid-p.png';
import img3 from '../public/gallery/Kokomi-p.png';
import img4 from '../public/gallery/Magical Word_crop-re.png';
import img5 from '../public/gallery/Christman outfit-nobg2-p.png';
import img6 from '../public/gallery/Ririco-p.png';
import img7 from '../public/gallery/Halloween-p.png';
import img8 from '../public/gallery/SVPRESIDENT-p1.png';
import img9 from '../public/gallery/Madoka-p.png';
import img10 from '../public/gallery/Towa-p.png';
import img11 from '../public/gallery/Miku-p.png';
import img12 from '../public/gallery/Halloween-Contest.png';
import img13 from '../public/gallery/Nadenade-p.png';
import img14 from '../public/gallery/Lumine-ipad-P.png';
import img15 from '../public/gallery/Fischl-p.png';
import img16 from '../public/gallery/Fanbox-start-p.png';
import img17 from '../public/gallery/Yume-post-sweet-space.png';
import img18 from '../public/gallery/Maid day-restored-p.png';
import img19 from '../public/gallery/Sakura-p.png';
import img20 from '../public/gallery/Kokone bunny girl-p.png';
import img21 from '../public/gallery/Tori-no-uta.png';
import { useRef, useState } from 'react';
import { GalleryPreview } from '../components/GalleryPreview';
import React from 'react';
import Head from 'next/head';

export interface StaticImageDataWithAlt {
  file: StaticImageData;
  alt: string;
  category: string;
}

const Gallery: NextPage = () => {
  const categories = ['ALL', 'ORIGINAL', 'FANART'];
  const [activeCategory, setActiveCategory] = useState('ALL');
  // import images using JS for easier resizing and image size detection
  const images: StaticImageDataWithAlt[] = [
    {
      file: img21,
      alt: "Illustration of Yume Tsuyuno. She's wearing a beautiful white summer dress and holding a Sunflower bouquet.",
      category: 'ORIGINAL'
    },
    {
      file: img20,
      alt: "Illustration of Kokone. She's wearing a sexy bunny girl outfit. She has long pink hair and yellow eyes. She's holding a tuff of her hair and looking at the viewer.",
      category: 'ORIGINAL'
    },
    {
      file: img19,
      alt: "Illustration of Sakura Kinomoto. She's wearing a sailor uniform and holding a cherry blossom flower. She's smiling and looking at the viewer. The background is a pink gradient.",
      category: 'FANART'
    },
    {
      file: img18,
      alt: "Illustration of Tsuyuno Yume. She's kneeling wearing a pink and white cute maid uniform. Kneeling, joining fingers into a heart. It is a masterpiece.",
      category: 'ORIGINAL'
    },
    {
      file: img17,
      alt: "Illustration of Tsuyuno Yume. She's sitting on a pink bed and eating a strawberry macaron in the foreground. In the background, there are translucent curtains and a window. The scene is backlit and gorgeous.",
      category: 'ORIGINAL'
    },
    {
      file: img16,
      alt: "Illustration of Tsuyuno Yume waving her hands hello and looking cute and happy.",
      category: 'ORIGINAL'
    },
    {
      file: img15,
      alt: "Illustration of Fischl from Genshin Impact. She's sitting on a red golden throne, looking down on the viewer in a cool pose.",
      category: 'FANART',
    },
    {
      file: img14,
      alt: 'Illustration of Lumine from Genshin Impact, surrounded by a swirl of wind and flying dandelions.',
      category: 'FANART',
    },
    {
      file: img13,
      alt: 'Illustration of Yume tsuyuno wearing pink dress, looking slightly sad, holding a cute mascot.',
      category: 'ORIGINAL',
    },
    {
      file: img1,
      alt: "Illustration of Yume Tsuyuno holding a headset she's wearing. for Maximaiser cover song by Hiiragi Magnetite.",
      category: 'ORIGINAL',
    },
    {
      file: img2,
      alt: 'Illustration of Yume Tsuyuno wearing a made outfit and slightly lifting her skirt',
      category: 'ORIGINAL',
    },
    {
      file: img3,
      alt: "Illustration of Kokomi from Genshin Impact game. She's facing toward the scenery and turning her head at the viewer.",
      category: 'FANART',
    },
    {
      file: img4,
      alt: 'Illustration of Yume Tsuyuno floating is a pink magical scenery',
      category: 'ORIGINAL',
    },
    {
      file: img5,
      alt: 'Illustration of Yume Tsuyuno wearing a christmas outfit',
      category: 'ORIGINAL',
    },
    {
      file: img6,
      alt: 'Illustration of Ririco Vtuber.',
      category: 'FANART',
    },
    {
      file: img7,
      alt: 'Illustration of a Yume Tsuyuno striking a cute pose wearing a witch hat and dress. Her eyes are slightly glowing.',
      category: 'ORIGINAL',
    },
    {
      file: img8,
      alt: 'Illustration of Yume Tsuyuno wearing a cute pink dress, and pouting',
      category: 'ORIGINAL',
    },
    {
      file: img9,
      alt: 'Illustration of Kaname Madoka from Puella Magi Madoka Magika anime.',
      category: 'FANART',
    },
    {
      file: img10,
      alt: 'Illustration of Touwa Vtuber',
      category: 'FANART',
    },
    {
      file: img11,
      alt: 'Illustration of Hatsune Miku',
      category: 'FANART',
    },
    {
      file: img12,
      alt: 'Illustration of a cute character wearing a sexy halloween outfit in a cute monster pose.',
      category: 'FANART',
    },
  ];

  /**
   * chooses closest matching ratio from aspect ratio list (landscape, portrait, and square)
   * @param img
   * @returns
   */
  const getAspectRatioClassName = (img: StaticImageDataWithAlt) => {
    // init possible ratios
    const landscapeRatio = 16 / 9;
    const portraitRatio = 1 / landscapeRatio;
    const squareRatio = 1;
    const ratios = [landscapeRatio, portraitRatio, squareRatio];
    const imgRatio = img.file.width / img.file.height;

    // calculate distance with every available ratio
    const distances: number[] = [];
    ratios.forEach((ratio) => {
      distances.push(Math.abs(imgRatio - ratio));
    });
    const smallestDistance = Math.min(...distances);
    const closestRatio = ratios[distances.indexOf(smallestDistance)];

    // return right classNames the closestRatio
    if (closestRatio == landscapeRatio)
      return 'w-full pointer-events-none md:pointer-events-auto md:w-1/2 aspect-landscape';
    if (closestRatio == portraitRatio)
      return 'w-full pointer-events-none md:pointer-events-auto md:w-1/6 aspect-portrait';
    if (closestRatio == squareRatio)
      return 'w-full pointer-events-none md:pointer-events-auto md:w-1/4 aspect-square';
    return 'w-full pointer-events-none md:pointer-events-auto md:w-1/4 aspect-square';
  };

  // state value is not accessible from inside a callback
  // its used to notify when DOM should rerender when its value changes
  const [selectedImage, selectImage] = useState(images[0]);

  // ref is used to access current value of state here
  // it is mutable, but changing its value will not trigger a rerender
  const selectedImageRef = useRef(selectedImage);
  selectedImageRef.current = selectedImage;

  const [isPreviewOpen, setPreviewState] = useState(false);

  const onImgClick = (img: StaticImageDataWithAlt) => {
    selectImage(img);
    setPreviewState(true);
  };

  const closePreview = () => {
    setPreviewState(false);
  };

  const onPreviewNext = () => {
    const currentImage = selectedImageRef.current;

    // find out index of this image in images array
    let imageIndex = 0;
    images.forEach((img, i) => {
      if (img.file.src == currentImage.file.src) imageIndex = i;
    });
    let newIndex = imageIndex + 1;
    if (newIndex >= images.length) newIndex = 0;
    console.log(newIndex);
    selectImage(images[newIndex]);
  };
  const onPreviewPrevious = () => {
    const currentImage = selectedImageRef.current;

    // find out index of this image in images array
    let imageIndex = 0;
    images.forEach((img, i) => {
      if (img.file.src == currentImage.file.src) imageIndex = i;
    });
    let newIndex = imageIndex - 1;
    if (newIndex < 0) newIndex = images.length - 1;
    console.log(newIndex);
    selectImage(images[newIndex]);
  };

  return (
    <div>
      <Head>
        <title>Yufimia - Gallery</title>
      </Head>
      <div className="rounded-xl overflow-hidden">
        <div className="flex justify-center p-4">
          {categories.map((category, key) => (
            <div
              key={key}
              onClick={() => {
                setActiveCategory(category);
              }}
              className={`px-6 py-2 hover:underline font-bold cursor-pointer transition-all text-white bg-yume-main-1 ${
                activeCategory == category && 'bg-yume-main-2'
              }`}
              style={{ fontFamily: "'M PLUS Rounded 1c', sans-serif" }}
            >
              {category}
            </div>
          ))}
        </div>
      </div>
      {/* desktop version */}
      <div className="hidden md:flex flex-wrap justify-center">
        {images
          .filter(
            (image) =>
              image.category == activeCategory || activeCategory == 'ALL'
          )
          .map((image, key) => (
            <div
              key={key}
              className="relative flex"
              style={{
                width: `${(image.file.width / image.file.height) * 320}px`,
                flexGrow: 1,
              }}
            >
              <Image
                onClick={() => onImgClick(image)}
                className="transition-transform duration-1000 hover:scale-110 cursor-pointer"
                src={image.file}
                placeholder="blur"
                objectFit="cover"
                alt={image.alt}
              />
            </div>
          ))}
      </div>
      {/* mobile version */}
      <div className="flex md:hidden flex-wrap justify-center">
        {images
          .filter(
            (image) =>
              image.category == activeCategory || activeCategory == 'ALL'
          )
          .map((image, key) => (
            <div
              key={key}
              className="relative flex"
              style={{
                width: `100%`,
                flexGrow: 1,
              }}
            >
              <Image
                onClick={() => onImgClick(image)}
                className="transition-transform duration-1000 hover:scale-110 cursor-pointer"
                src={image.file}
                placeholder="blur"
                objectFit="cover"
                alt={image.alt}
              />
            </div>
          ))}
      </div>
      <GalleryPreview
        isOpen={isPreviewOpen}
        img={selectedImage}
        onClose={closePreview}
        onNext={onPreviewNext}
        onPrevious={onPreviewPrevious}
      />
    </div>
  );
};

export default Gallery;
