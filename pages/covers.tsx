import type { NextPage } from 'next';
import bgImg from '../public/banners/home/desktop-bg.jpg';
import Image from 'next/image';
import React from 'react';
import Head from 'next/head';

const covers: Cover[] = [
  {
    title: '鳥の詩 ／ Lia',
    url: "https://www.youtube-nocookie.com/embed/S-ry0j8hUBw"
  },
  {
    title: 'SOS #シャニマス歌ってみた',
    url: "https://www.youtube-nocookie.com/embed/77BkzKIsdOs"
  },
  {
    title: '撫でんな ／ 柊マグネタイト',
    url: 'https://www.youtube-nocookie.com/embed/D7SCHkd6Wn4',
  },
  {
    title: 'マーシャル・マキシマイザー ／ 柊マグネタイト',
    url: 'https://www.youtube-nocookie.com/embed/VJKusggomaE',
  },
  {
    title: 'Magical Word ／ P丸様。',
    url: 'https://www.youtube-nocookie.com/embed/DEVTiyGV3GM',
  },
  {
    title: 'アニマル ／ DECO*27',
    url: 'https://www.youtube-nocookie.com/embed/R1cLIlrryk4',
  },
  {
    title: 'シル・ヴ・プレジデント ／ P丸様。',
    url: 'https://www.youtube-nocookie.com/embed/qDXGnA5omd8',
  },
  {
    title: 'シンデレラ ／ DECO*27',
    url: 'https://www.youtube-nocookie.com/embed/YAR-7i_Dobw',
  },
  {
    title: 'ヴァンパイア ／ DECO*27',
    url: 'https://www.youtube-nocookie.com/embed/OVnqh8fPae8',
  },
  {
    title: 'No.1 ／ HoneyWorks',
    url: 'https://www.youtube-nocookie.com/embed/chrxt2dGgkE',
  },
];

const Covers: NextPage = () => {
  return (
    <div className="flex-grow pt-5">
      <Head>
        <title>Yufimia - Covers</title>
      </Head>
      <div className="fixed -z-10 top-0 left-0 right-0 bottom-0">
        <Image
          src={bgImg}
          layout="fill"
          objectFit="cover"
          objectPosition="center left"
          style={{
            pointerEvents: 'none',
          }}
          alt=""
        />
      </div>
      <div className="flex flex-wrap max-w-6xl mx-auto">
        {covers.map((cover, key) => (
          <div key={key} className="w-full md:w-1/3 p-3">
            <div className="bg-white p-3 shadow-lg">
              <div className="aspect-landscape">
                <iframe
                  src={`${cover.url}?rel=0`}
                  title="YouTube video player"
                  allow="fullscreen; accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  style={{ width: '100%', height: '100%' }}
                ></iframe>
              </div>
              <header className="text-center mt-2">{cover.title}</header>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
export default Covers;

export interface Cover {
  title: string;
  url: string;
}
