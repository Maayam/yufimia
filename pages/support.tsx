import type { NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import { useEffect, useState } from 'react';
import React from 'react';
import socialItems from '../content/socials';
import twitterIcon from '../public/sns/twitter.svg';
import bandcampImg from '../public/support/bandcamp-support.jpg';
import boothImg from '../public/support/booth-support.jpg';
import fanboxImg from '../public/support/fanbox-support.jpg';

const About: NextPage = () => {
  const cards = [
    {
      name: 'BOOTH',
      href: "https://yufimia.booth.pm/",
      thumb: boothImg
    },
    {
      name: 'FANBOX',
      href: "https://tsuyuno-yume.fanbox.cc/",
      thumb: fanboxImg
    },
    {
      name: 'Bandcamp',
      href: "https://yufimia.bandcamp.com/",
      thumb: bandcampImg
    },
  ];

  const iframeStyling = {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    maxWidth: "100%"
  };

  const getBtnStyle = (lang: string) => {
    if (lang === formLanguage) {
      return "bg-white font-bold py-1 px-4";
    } else {
      return "bg-white py-1 px-4 opacity-60";
    }
  }

  const [formLanguage, setFormLanguage] = useState("EN");
  return (
    <div className="flex-grow flex flex-col items-center pt-5 bg-yume-main-1">
      <Head>
        <title>Yufimia - About</title>
      </Head>

      <div className="flex-grow flex flex-wrap justify-around items-center p-3 w-full md:w-2/3 py-20">
        {cards.map((member, key) => (
          <div
            key={key}
            className="w-72 flex flex-col sm:flex-row sm:items-start w-full p-3 bg-white rounded-md shadow mb-3"
          >
            <Link key={key} href={member.href} target="_blank">
              <div className="cursor-pointer flex flex-col items-center">
                <a
                  key={key}
                  className={`mb-2 bg-yume-main-1 rounded-lg py-2 px-4 text-white font-bold`}
                  target="_blank"
                >
                  {member.name}
                </a>
                <Image src={member.thumb} />
              </div>
            </Link>
          </div>
        ))}
      </div>

      {/* contactForm Switcher */}
      <div>
        {/* beautiful formLanguage switcher button */}
        <div className='flex'>
          <button className={getBtnStyle("EN")} onClick={()=>setFormLanguage("EN")}>
            EN
          </button>
          <button className={getBtnStyle("JP")} onClick={()=>setFormLanguage("JP")}>
            日本語
          </button>
        </div>
        {formLanguage === "EN" && (<iframe width="540" height="440" src="https://116da1ca.sibforms.com/serve/MUIFAIgbMdMu9xt_l4oAa1b4GDYkrR1-8yw6ao_Wf2nfbD83G6a2qerE19ZAG_IP9fq2jIUsgLqVdQPog7tTgJI6N8cjb3SqeyUdrIpiLoCnVo0fAnjcdQo1zIpzsABVIlDhBZq5JYx7Y4WQZYFFLzbE-_5EWv69K0vUwY9KjYiFJdStZkGsxeYygWhdtZcANqmNyXcO4fjfJVoA" frameBorder="0" scrolling="auto" allowFullScreen style={iframeStyling}></iframe>)}
        {formLanguage === "JP" && (<iframe width="540" height="440" src="https://116da1ca.sibforms.com/serve/MUIFADuzs4dWiGlKfxfHhu-fv199D93A9IqTYSsOfj6nToFPPa2WWxI4-5LypLXV-iUfLEAnwxucHihfUY4A7Oisb3OsMhfY5DkWOfNj9WNqp10GIMuQMCVUxCJA_DKj0ECmEaCQCalZdcb_V6ecjOXfqhW88o6kNG4U-8HBVTARy92dNAdgLnZDQ5FNOXpioIkY4E_rj3H2R_Gn" frameBorder="0" scrolling="auto" allowFullScreen style={iframeStyling}></iframe>)}
      </div>
    </div>
  );
};

export default About;
