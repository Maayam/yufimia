import type { NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import { useEffect, useState } from 'react';
import React from 'react';
import socialItems from '../content/socials';
import twitterIcon from '../public/sns/twitter.svg';

const About: NextPage = () => {
  setTimeout(()=>{
    // Nextjs navigation to home
    window.location.href = "/";
  }, 4000)
  return (
    <div className="flex-grow flex flex-col items-center pt-5 bg-yume-main-1">
      <Head>
        <title>Yufimia - About</title>
      </Head>
      <div className="flex-grow flex flex-wrap justify-around items-center p-3 w-full md:w-1/2 max-w-3xl py-20">
          <div
            className="flex flex-col sm:flex-row sm:items-start w-full p-3 bg-white rounded-md shadow mb-3"
          >
            <div className="pl-5">
              <header className="text-2xl">Thank you for subscribing!</header>
              <header className="text-2xl mb-2">ご登録いただき、ありがとうございます！</header>
              <Link href="/">
                <a
                  className={`bg-yume-main-1 rounded-lg py-2 px-4 text-white font-bold`}
                >
                  Back
                </a>
              </Link>
            </div>
          </div>
      </div>
    </div>
  );
};

export default About;
