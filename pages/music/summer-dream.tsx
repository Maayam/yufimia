import type { NextPage } from 'next';
import React from 'react';
import { SummerDreamBanner } from '../../components/SummerDreamBanner';

const Home: NextPage = () => {
  return <SummerDreamBanner />;
};


export default Home;
