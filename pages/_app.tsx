import '../styles/globals.css';
import type { AppProps } from 'next/app';
import { MainLayout } from '../components/MainLayout';
import { useEffect, useState } from 'react';
import React from 'react';

function MyApp({ Component, pageProps }: AppProps) {
  useEffect(() => {
    document.querySelector('#app-loader')?.classList.remove('opacity-100');
    document.querySelector('#app-loader')?.classList.add('opacity-0');
    document
      .querySelector('#app-loader')
      ?.classList.remove('pointer-events-auto');
    document.querySelector('#app-loader')?.classList.add('pointer-events-none');
  }, []);

  return (
    <>
      <MainLayout>
        <Component {...pageProps} />
      </MainLayout>
    </>
  );
}

export default MyApp;
