import type { NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import React from 'react';
import socialItems from '../content/socials';
import twitterIcon from '../public/sns/twitter.svg';

const team = [
  {
    name: 'つゆのゆめ / Yume Tsuyuno',
    title: 'Virtual singer - Illustrator',
    avatar:
      'https://pbs.twimg.com/profile_images/1529807220497698817/_rDTdloN_400x400.jpg',
    bio: {
      __html: `
    異世界の魔法少女、今はバーチャルメイド。<br>
    大好きな歌をたくさんの人に届けるのが夢。<br>
    <br>
    誕生日：２日７月<br>
    目標：３Ｄで自由に踊ること<br>
    <br>
    <hr>
    <br>
    A magical girl who came from another world, now working as a virtual maid.<br>
    She loves singing, and wants her songs to reach a lot of people.<br>
    <br>
    Birthday: 2nd July<br>
    Goal: To dance freely in 3D`,
    },
    socials: socialItems,
  },
  {
    name: 'Mitune_e',
    title: 'Mixing - Video',
    avatar:
      'https://pbs.twimg.com/profile_images/1728857179195580417/IY4ZVuyI_400x400.jpg',
    bio: {
      __html: `昼はソフトウェアエンジニア、夜は創作活動に熱心！<br>
      作曲、ミックス、動画編集などを担当している。<br>
      <br>
      <hr>
      <br>
      Full time software engineer during the day, and passionate producer at night.<br>
      Mitune_e combines all kind of software and techniques together to produce interesting Music Videos (MV).<br>
      He specializes in vocals mixing and lyrics video production for song covers and original songs.`,
    },
    socials: [
      {
        title: 'twitter',
        iconSrc: twitterIcon,
        url: 'https://twitter.com/Mitune_e',
      },
    ],
  },
];

const About: NextPage = () => {
  return (
    <div className="flex-grow flex flex-col items-center pt-5 bg-yume-main-1">
      <Head>
        <title>Yufimia - About</title>
      </Head>
      <div className="flex-grow flex flex-wrap justify-around items-center p-3 w-full md:w-1/2 max-w-3xl py-20">
        {team.map((member, key) => (
          <div
            key={key}
            className="flex flex-col sm:flex-row sm:items-start w-full p-3 bg-white rounded-md shadow mb-3"
          >
            <div className="relative w-48 sm:w-1/3 md:w-1/3 xl:w-1/4 aspect-square rounded-full overflow-hidden flex-shrink-0">
              <Image
                src={member.avatar}
                placeholder="blur"
                blurDataURL={member.avatar}
                layout="fill"
                objectFit="cover"
                className="rounded-full w-36 border-4 border-gray-500 aspect-square object-cover flex-shrink-0"
                alt=""
              />
            </div>
            <div className="pl-5">
              <header className="text-2xl">{member.name}</header>
              <header className="text-lg mb-5">{member.title}</header>
              <div className="flex mb-3">
                {member.socials.map((item, key) => (
                  <Link key={key} href={item.url}>
                    <a
                      target="blank"
                      className="relative block px-2 transition-transform hover:scale-110"
                      style={{ maxWidth: '48px' }}
                    >
                      <Image
                        src={item.iconSrc}
                        objectFit="contain"
                        width="100%"
                        height="100%"
                        alt={item.title}
                      />
                    </a>
                  </Link>
                ))}
              </div>
              <p dangerouslySetInnerHTML={member.bio}></p>
              {member.name === 'つゆのゆめ / Yume Tsuyuno' && (
                <div className="mt-3 flex gap-2">
                  <Link href="/covers">
                    <a
                      className={`bg-yume-main-2 rounded-lg py-2 px-4 text-white font-bold`}
                    >
                      Song Covers
                    </a>
                  </Link>
                  <Link href="/gallery">
                    <a
                      className={`bg-yume-main-2 rounded-lg py-2 px-4 text-white font-bold`}
                    >
                      Illustrations
                    </a>
                  </Link>
                </div>
              )}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default About;
