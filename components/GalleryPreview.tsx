import Image, { StaticImageData } from 'next/image';
import React from 'react';
import { StaticImageDataWithAlt } from '../pages/gallery';

export interface GalleryPreviewProps {
  isOpen: boolean;
  img: StaticImageDataWithAlt;
  onClose: Function;
  onNext: Function;
  onPrevious: Function;
}

export const GalleryPreview: React.FC<GalleryPreviewProps> = ({
  isOpen,
  img,
  onClose,
  onNext,
  onPrevious,
}) => {
  const imageStyle = {};
  const previewStyle = {};
  const close = () => onClose();
  const previous = () => onPrevious();
  const next = () => onNext();
  return (
    <div className={`${isOpen ? 'block' : 'hidden'} select-none`}>
      {/* preview container */}
      <div
        style={{
          height: '100vh',
          width: '100vw',
          ...previewStyle,
        }}
        className="
          fixed
          top-0
          left-0
          z-50
          bg-white
          px-5
          py-3
          rounded-md
          flex
          flex-col
          items-center
        "
      >
        <div className="flex justify-end w-full relative z-50">
          <button onClick={close}>close</button>
        </div>
        <div
          onClick={previous}
          className="absolute top-0 left-0 bottom-0 bg-white transition-all opacity-0 hover:opacity-50 cursor-pointer w-1/3 z-40 flex justify-center items-center text-4xl"
        >
          &lt;
        </div>
        <Image
          src={img.file}
          objectFit="contain"
          layout="intrinsic"
          alt={img.alt}
        />
        <div
          onClick={next}
          className="absolute top-0 right-0 bottom-0 bg-white transition-all opacity-0 hover:opacity-50 cursor-pointer w-1/3 z-40 flex justify-center items-center text-4xl"
        >
          &gt;
        </div>
      </div>
    </div>
  );
};
