import Image from 'next/image';
import Link from 'next/link';
import { useState } from 'react';
import React from 'react';
import socialItems from '../content/socials';
import yufimiaLogo from '../public/yufimia-logo.svg'

export interface NavMenuItem {
  title: string;
  url: string;
  target?: string;
}

export const Topbar: React.FC<TopBarProps> = () => {
  const noTabIndex: number = -1;
  const navItems: NavMenuItem[] = [
    { title: 'About', url: '/about' },
    // { title: 'Covers', url: '/covers' },
    // { title: 'Gallery', url: '/gallery' },
    { title: 'Music', url: '/music/summer-dream' },
    // { title: 'Shop', url: 'https://tsuyuno-yume.booth.pm/', target: 'blank' },
    // { title: 'Show support', url: 'https://tsuyuno-yume.fanbox.cc/', target: 'blank' }
    { title: 'Show support', url: '/support' },
    { title: 'Contact', url: 'mailto:yufimia.contact@gmail.com'}
  ];

  const snsIconStyle = {
    display: 'flex',
    height: '35px',
    width: '0%',
  };

  const [burgerOpen, setBurgerState] = useState(false);

  const burgerOpenStyle = burgerOpen ? '' : 'translate-x-full';
  const pointerAuto = 'auto';
  const pointerNone = 'none';

  const getPointerEvents = (bool: boolean): 'auto' | 'none' => {
    return burgerOpen ? 'auto' : 'none';
  };

  const backDropStyle = {
    pointerEvents: getPointerEvents(burgerOpen),
    backgroundColor: 'black',
    opacity: `${burgerOpen ? '0.5' : '0'}`,
  };

  const closeMenu = () => {
    setBurgerState(false);
  };
  const onMenuClick = () => {
    setBurgerState(!burgerOpen);
  };

  return (
    <nav
      className="flex justify-center md:justify-between items-center p-3 sticky top-0 z-40 shadow-lg"
      style={{ backgroundColor: '#fff' }}
    >
      {/* brand */}
      <Link href="/">
        <a
          className="flex w-48 h-8 text-yume-dark text-3xl"
          style={{
            fontFamily: 'Arima, cursive',
            fontWeight: '200',
            letterSpacing: '12px',
          }}
        >
          <Image
            objectFit='contain'
            src={yufimiaLogo}
          />
        </a>
      </Link>
      <div
        className={`
          fixed
          top-0
          right-0
          bottom-0
          z-20
          flex
          flex-col
          justify-between
          w-72
          pt-16
          transition-transform
          ${burgerOpenStyle}
          duration-300
          md:static
          md:flex-row
          md:pt-0
          md:transform-none
          md:flex-grow
        `}
        style={{
          backgroundColor: 'white',
          fontFamily: "'M PLUS Rounded 1c', sans-serif",
        }}
      >
        {/* nav menu */}
        <div className="flex flex-col items-end md:flex-row md:items-center md:justify-center md:pl-7">
          {navItems.map((item, key) => (
            <Link key={key} href={item.url} target={item.target}>
              <a
                key={key}
                onClick={closeMenu}
                className="pr-6 md:px-4 p-2 text-yume-dark text-xl md:text-lg flex relative"
                target={item.target}
              >
                {key != 0 && (
                  <div
                    className="top-0 bottom-0 right-2 transform rotate-90 -translate-y-5 -translate-x-9 md:transform-none md:block bg-yume-dark absolute md:right-auto md:top-2 md:bottom-2 md:left-0"
                    style={{ width: '0.5px' }}
                  ></div>
                )}
                <div>{item.title}</div>
              </a>
            </Link>
          ))}
        </div>
        {/* social menu */}
        <div className="flex md:w-64 pb-5 pl-5 pr-3 md:p-0 items-center">
          {socialItems.map((item, key) => (
            <Link key={key} href={item.url}>
              <a
                target="blank"
                className="relative block px-2 transition-transform hover:scale-110"
                style={{ maxWidth: '64px' }}
              >
                <Image
                  src={item.iconSrc}
                  objectFit="contain"
                  width="100%"
                  height="100%"
                  alt={item.title}
                />
              </a>
            </Link>
          ))}
        </div>
      </div>
      {/* burger menu */}
      <div
        onClick={onMenuClick}
        className="absolute right-5 z-30 md:hidden w-12 h-10 p-2 border-opacity-0 border-2 border-yume-dark  focus:border-opacity-100 transition-all flex flex-col justify-between rounded-md "
        tabIndex={noTabIndex}
      >
        <div className="w-full bg-yume-dark" style={{ height: '1px' }}></div>
        <div className="w-full-0 bg-yume-dark" style={{ height: '1px' }}></div>
        <div className="w-full bg-yume-dark" style={{ height: '1px' }}></div>
      </div>
      <div
        onClick={closeMenu}
        className="fixed top-0 left-0 right-0 bottom-0 opacity-50 z-10 transition-all duration-300 md:left-full"
        style={backDropStyle}
      ></div>
    </nav>
  );
};

export interface TopBarProps {}
