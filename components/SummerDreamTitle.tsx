// summer dream title component

import React, { useState } from 'react';
import Head from 'next/head';

export const SummerDreamTitle: React.FC<SummerDreamTitleProps> = (props) => {

    const { color } = props || '#5493CC';

    let dashArrayDistance: Record<string, number> = {
        s: 225,
        umm: 855,
        er: 220,
        d1: 100,
        d2: 200,
        r: 120,
        e: 145,
        a: 180,
        m: 300,
        line: 767
    }
    const maxDashArray = '9999px';

    const [s, sets] = useState(`0px ${maxDashArray}`)
    const [umm, setumm] = useState(`0px ${maxDashArray}`)
    const [er, seter] = useState(`0px ${maxDashArray}`)
    const [d1, setd1] = useState(`0px ${maxDashArray}`)
    const [d2, setd2] = useState(`0px ${maxDashArray}`)
    const [r, setr] = useState(`0px ${maxDashArray}`)
    const [e, sete] = useState(`0px ${maxDashArray}`)
    const [a, seta] = useState(`0px ${maxDashArray}`)
    const [m, setm] = useState(`0px ${maxDashArray}`)
    const [line, setline] = useState(`0px ${maxDashArray}`)
    const [special, setSpecial] = useState(`0px ${maxDashArray}`)

    // stroke distance in pixel for each svg part
    // stroke speed in pixels per second
    let strokeSpeed = 500;

    // compute stroke duration based on distance and speed
    let strokeDuration: Record<string, number> = {}
    for (let key in dashArrayDistance) {
        strokeDuration[key] = dashArrayDistance[key] / strokeSpeed;
    }
    
    let strokeDelay: Record<string, number> = {}
    // compute strokedelay based on stroke duration. First delay is 0, and next delay = previous delay + current duration
    let previousDelay = 0;
    for (let key in strokeDuration) {
        strokeDelay[key] = previousDelay;
        previousDelay += strokeDuration[key];
    }
    
    // paint strokes when component is mounted
    React.useEffect(() => {
        setTimeout(() => {
            sets( `${dashArrayDistance['s']}px ${maxDashArray}` )
            setumm( `${dashArrayDistance['umm']}px ${maxDashArray}` )
            seter( `${dashArrayDistance['er']}px ${maxDashArray}` )
            setd1( `${dashArrayDistance['d1']}px ${maxDashArray}` )
            setd2( `${dashArrayDistance['d2']}px ${maxDashArray}` )
            setr( `${dashArrayDistance['r']}px ${maxDashArray}` )
            sete( `${dashArrayDistance['e']}px ${maxDashArray}` )
            seta( `${dashArrayDistance['a']}px ${maxDashArray}` )
            setm( `${dashArrayDistance['m']}px ${maxDashArray}` )
            setline( `${dashArrayDistance['line']}px ${maxDashArray}`)
        }, 100);
    }, []);

    return (
        <div className="w-full">
            <svg width="100%" height="223" viewBox="0 0 803 223" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path style={{
                   strokeDashoffset:'1px',
                   transitionTimingFunction: 'ease-out',
                    transition: `${strokeDuration.line}s`,
                    transitionDelay: `${strokeDelay.line}s`,
                    strokeDasharray: line,
                    stroke: color
             }} d="M33 221.234C122.5 186.734 365.356 118.225 617.5 88.7343C703 78.7343 788.5 84.7344 761 105.234" strokeWidth="2" strokeLinecap="round" />
                <path style={{
                   strokeDashoffset:'1px',
                   transitionTimingFunction: 'ease-out',
                    transition: `${strokeDuration.m}s`,
                    transitionDelay: `${strokeDelay.m}s`,
                    strokeDasharray: m,
                    stroke: color
             }} d="M713.521 18.4295C712.159 27.4161 711.138 32.4918 706.521 41.9295C704.379 51.8837 702.694 57.225 699.021 66.4295C708.688 48.2628 731.237 11.2902 744.021 11.9295C754.021 12.4296 741.521 35.4296 738.021 50.9295C747.521 36.4295 766.521 9.42961 771.521 7.42961C775.699 5.75835 757.307 46.314 770.521 53.4295C777.021 56.9296 796.021 37.9296 801.021 29.4295" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                <path style={{
                   strokeDashoffset:'1px',
                   transitionTimingFunction: 'linear',
                    transition: `${strokeDuration.a}s`,
                    transitionDelay: `${strokeDelay.a}s`,
                    strokeDasharray: a,
                    stroke: color
             }} d="M681.521 32.4296C681.248 29.9961 683.33 24.1086 675.521 23.4296C664.021 22.4297 650.854 42.763 646.521 52.4296C645.188 57.9296 643.922 67.9097 649.021 68.9296C656.521 70.4297 673.021 55.4297 680.521 39.4296C675.521 49.4297 669.565 61.8227 677.021 64.9296C683.021 67.4297 700.521 50.9297 706.521 41.9296" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                <path style={{
                   strokeDashoffset:'1px',
                   transitionTimingFunction: 'linear',
                    transition: `${strokeDuration.e}s`,
                    transitionDelay: `${strokeDelay.e}s`,
                    strokeDasharray: e,
                    stroke: color
             }} d="M606.521 49.4295C620.521 49.4296 632.021 38.9296 632.521 32.4295C633.349 21.6613 623.817 24.691 618.521 29.4295C609.021 37.9296 598.427 59.9886 605.021 70.4296C611.021 79.9297 632.021 70.9297 648.521 48.9297" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                <path style={{
                   strokeDashoffset:'1px',
                   transitionTimingFunction: 'linear',
                    transition: `${strokeDuration.r}s`,
                    transitionDelay: `${strokeDelay.r}s`,
                    strokeDasharray: r,
                    stroke: color
             }} d="M571.021 37.4295C571.706 39.3711 561.906 68.3042 556.021 81.9295C566.887 62.0717 583.88 33.2027 597.521 30.9295C600.521 30.4296 601.021 33.9296 600.521 35.4296" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                <path style={{
                   strokeDashoffset:'1px',
                   transitionTimingFunction: 'linear',
                    transition: `${strokeDuration.d2}s`,
                    transitionDelay: `${strokeDelay.d2}s`,
                    strokeDasharray: d2,
                    stroke: color
             }} d="M471.521 2.92968C491.449 -1.07164 550.521 6.92968 547.021 44.9297C544.296 74.5113 466.021 98.4296 459.021 92.9296" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                <path style={{
                   strokeDashoffset:'1px',
                   transitionTimingFunction: 'linear',
                    transition: `${strokeDuration.d1}s`,
                    transitionDelay: `${strokeDelay.d1}s`,
                    strokeDasharray: d1,
                    stroke: color
             }} d="M480.021 6.92969C471.188 32.9297 446.521 91.4297 452.521 96.4297" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                <path style={{
                   strokeDashoffset:'1px',
                   transitionTimingFunction: 'linear',
                    transition: `${strokeDuration.er}s`,
                    transitionDelay: `${strokeDelay.er}s`,
                    strokeDasharray: er,
                    stroke: color
             }} d="M336.998 82.4943C345.533 82.9891 362.998 73.4943 363.498 63.4943C363.695 59.5455 358.921 56.8821 354.498 59.9943C344.046 67.3486 336.772 77.2971 336.998 82.4943ZM336.998 82.4943C333.984 88.0912 333.242 102.497 337.498 104.994C355.525 115.572 387.498 78.4943 387.498 59.4944C382.374 78.6369 378.269 88.2699 372.498 104.494C380.831 87.161 402.651 55.7065 413.498 52.9944C415.498 52.4943 417.498 54.9943 416.498 56.9944" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                <path style={{
                   strokeDashoffset:'1px',
                   transitionTimingFunction: 'linear',
                    transition: `${strokeDuration.umm}s`,
                    transitionDelay: `${strokeDelay.umm}s`,
                    strokeDasharray: umm,
                    stroke: color
             }} d="M93.5212 97.4297C87.8545 110.93 79.3019 135.999 89.5212 137.93C100.774 140.055 117.938 110.131 123.521 90.9298C117.854 106.43 110.359 140.297 119.521 137.43C135.05 132.569 154.021 101.93 154.021 86.9298C149.186 106.639 146.212 117.206 140.021 134.43C149.854 116.43 172.638 79.7781 185.021 80.4298C194.521 80.9297 179.521 107.43 178.521 120.43C190.613 101.348 197.699 91.3798 211.521 76.4298C212.521 87.4297 199.213 120.071 211.021 122.43C226.336 125.489 248.021 93.4296 248.521 75.4297C243.955 95.2586 240.747 105.727 234.021 123.43C243.453 106.426 265.153 68.6241 279.021 68.9298C287.957 69.1267 276.258 96.3709 272.521 108.43C284.347 90.4279 292.011 80.8424 306.021 64.9297C305.521 78.9297 292.021 111.43 307.021 110.93C317.016 110.597 331.521 92.9297 337.021 82.9297" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                <path style={{
                   strokeDashoffset:'1px',
                   transitionTimingFunction: 'ease-in',
                    transition: `${strokeDuration.s}s`,
                    transitionDelay: `${strokeDelay.s}s`,
                    strokeDasharray: s,
                    stroke: color
             }} d="M69.0211 80.4297C72.0211 78.9297 78.0211 74.3297 78.0211 67.9297C78.0211 59.9297 73.0211 57.9297 70.0211 57.9297C55.5211 57.9297 27.0211 69.9297 27.0211 85.4297C27.0211 100.93 69.212 93.9398 68.5211 117.43C68.0211 134.43 52.4427 144.574 35.0211 148.93C27.0211 150.93 -3.97906 146.429 3.02102 125.43" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </div>
    );

}

export interface SummerDreamTitleProps {
    color?: string;
}