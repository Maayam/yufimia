import React, {
  MouseEventHandler,
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react';
import { Particle } from './Particle';

type Props = {
  children: JSX.Element;
};

interface cords {
  x: number;
  y: number;
}

const timeInterval = 80;

export const PointerParticles: React.FC<Props> = ({ children }) => {
  const strength = 100;

  const shouldEmitRef = useRef(false);
  const [shouldEmit, setShouldEmit] = useState(shouldEmitRef.current);

  const [origins, setOrigins] = useState([{ x: 0, y: 0 }]);

  const particle: Particle = {
    x: 0,
    y: 0,
    translate: '30px, 300px',
    rotate: '90deg',
    birthDate: 0,
  };
  const [particles, setParticles] = useState([particle]);
  const particlesRef = useRef(particles);

  const emitParticles = useCallback(
    (amount = 1) => {
      if (shouldEmit) {
        const newParticles: Particle[] = [];
        origins.forEach((origin) => {
          for (let i = 0; i < amount; i++) {
            const xSpeed = Math.round(Math.random() * strength);
            const ySpeed = Math.round(Math.random() * strength);
            const rotate = Math.round(Math.random() * 180);
            const newParticle: Particle = {
              x: origin.x,
              y: origin.y,
              translate: `${xSpeed}px, ${ySpeed}px`,
              rotate: `${rotate}deg`,
              birthDate: Date.now(),
            };
            newParticles.push(newParticle);
          }
        });
        return newParticles;
      }
    },
    [origins, shouldEmit]
  );

  const killParticles = useCallback(() => {
    const newParticles = particles.filter(
      (particle) => Date.now() - particle.birthDate < 1000
    );
    setParticles(newParticles);
  }, [particles]);

  const stopEmitter = () => {
    setShouldEmit(false);
  };
  const startEmitter = () => {
    setShouldEmit(true);
  };
  const updateOrigins = (e: MouseEvent | TouchEvent) => {
    if ('touches' in e) {
      const touches = Array.from(e.touches);
      const origins = touches.map((touch) => ({
        x: touch.clientX,
        y: touch.clientY,
      }));
      setOrigins(origins);
    }
    if ('clientX' in e && 'clientY' in e) {
      const origin = { x: e.clientX, y: e.clientY };
      setOrigins([origin]);
    }
  };
  const onMouseDown = (e: React.MouseEvent<HTMLDivElement>) => {
    updateOrigins(e.nativeEvent);
    startEmitter();
    emitParticles(15);
  };
  const onMouseMove = (e: React.MouseEvent<HTMLDivElement>) => {
    updateOrigins(e.nativeEvent);
  };
  const onMouseUp = (e: React.MouseEvent<HTMLDivElement>) => {
    setOrigins([]);
    stopEmitter();
  };
  const onTouchStart = (e: React.TouchEvent<HTMLDivElement>) => {
    updateOrigins(e.nativeEvent);
    startEmitter();
    emitParticles(15);
  };
  const onTouchMove = (e: React.TouchEvent<HTMLDivElement>) => {
    updateOrigins(e.nativeEvent);
  };
  const onTouchEnd = (e: React.TouchEvent<HTMLDivElement>) => {
    setOrigins([]);
    stopEmitter();
  };

  const pointerParticlesProps = {
    onMouseDown,
    onMouseMove,
    onMouseUp,
    onTouchStart,
    onTouchMove,
    onTouchEnd,
  };

  useEffect(() => {
    const particlesInterval = () => {
      const parts = emitParticles();
      setParticles(parts || []);
      killParticles();
    };
    let particlesLoop = setInterval(particlesInterval, timeInterval);

    return () => {
      clearInterval(particlesLoop);
    };
  }, [emitParticles, killParticles, shouldEmit, particles]);
  return (
    <div className="h-1/6 flex-grow flex flex-col" {...pointerParticlesProps}>
      <div className="fixed top-0 left-0 pointer-events-none z-50">
        <div>{shouldEmit.toString()}</div>
        <div>{particles.length}</div>
        {particles.map((particle, key) => (
          <Particle key={key} data={particle} />
        ))}
      </div>
      {children}
    </div>
  );
};
