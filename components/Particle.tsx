import React, { MouseEventHandler, useEffect, useState } from 'react';

type Props = {
  data: Particle;
};

export interface Particle {
  x: number;
  y: number;
  translate: string;
  rotate: string;
  birthDate: number;
}

export const Particle: React.FC<Props> = ({ data }) => {
  const positionStyle = {
    left: data.x,
    top: data.y,
  };
  const [animationStyles, setAnimationStyles] = useState({});
  useEffect(() => {
    setAnimationStyles({
      transformOrigin: 'center center',
      transform: `translate(${data.translate}) rotate(${data.rotate})`,
    });
  }, []);
  return (
    <div
      className="w-2 h-2 bg-green-500 transition-all duration-1000"
      style={{ ...positionStyle, ...animationStyles }}
    ></div>
  );
};
