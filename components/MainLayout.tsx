import { Topbar } from './topbar';
import React, { useEffect, useState } from 'react';

type Props = {
  children: JSX.Element;
};

// this needs to be defined outside of component creation or it will create an infinite loop
const showStyle = {
  opacity: 1,
  // transform: 'scale(1)',
  // transformOrigin: 'top center',
};
const hideStyle = {
  opacity: 0,
  // transform: 'scale(0.95)',
  // transformOrigin: 'top center',
};
// gallery preview is not fixed anymore if this is set

export const MainLayout: React.FC<Props> = ({ children }) => {
  // store passed children in new property. We will display this new property and not the one that is passed
  // this way, we can control *when* displayed children change
  const [displayChildren, setDisplayChildren] = useState(children);

  const displayClasses = 'transition duration-500';
  const [transitionStage, setTransitionStage] = useState(hideStyle);

  useEffect(() => {
    // when component loads, hide it
    if (children !== displayChildren) setTransitionStage(hideStyle);
  }, [setTransitionStage, children, displayChildren]); // I dont really get what those dependencies array means for now

  // when hiding transition ends, replace displayChildren with currently passed children
  // set showStyle to display container again
  const onTransitionEnd = () => {
    if (transitionStage.opacity === 0) {
      setDisplayChildren(children);
      setTransitionStage(showStyle);
    }
  };

  return (
    <div className="flex flex-col h-1/6 flex-grow">
      <main className="h-1/6 flex-grow flex flex-col">
        <Topbar />
        <div
          onTransitionEnd={onTransitionEnd}
          className={`${displayClasses} flex-grow flex flex-col`}
          style={transitionStage}
        >
          {displayChildren}
        </div>
      </main>
    </div>
  );
};
