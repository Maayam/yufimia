import Image from 'next/image';
import bgImg from '../public/banners/home/desktop-bg.jpg';
import farImg from '../public/banners/home/desktop-far.png';
import charImg2 from '../public/banners/home/desktop-char2.png';
import midImg from '../public/banners/home/desktop-mid.png';
import ratherCloseImg from '../public/banners/home/desktop-rather-close.png';
import closeImg from '../public/banners/home/desktop-close.png';
import veryCloseImg from '../public/banners/home/desktop-very-close.png';
import { useEffect, useState } from 'react';
import React from 'react';
import Head from 'next/head';

export const HomeBanner: React.FC<HomeBannerProps> = () => {
  let currentValue = 0;
  const [coefX, setCoefX] = useState(0);
  const [coefY, setCoefY] = useState(0);

  let setCoefTimeout: NodeJS.Timeout;

  const onMouseMove = throttle<React.MouseEvent<HTMLDivElement, MouseEvent>>(
    (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
      const { clientX, clientY } = e;
      const { innerWidth, innerHeight } = window;
      // we want to scale posX and posY like number between -1 and 1
      // -1 = far left/top +1 = far right/bottom
      // clientX / innerWidth gets ratio between 0 and 1
      // * 2 gets number between 0 and 2
      // -1 gets number between -1 and 1
      const x = (clientX / innerWidth) * 2 - 1;
      const y = (clientY / innerHeight) * 2 - 1;
      setCoefX(x);
      setCoefY(y);
    },
    1000 / 30
  );

  /**
   *
   * @param amplitude amplitude of movement in px
   * @param coefX % of movement that should be applied
   * @param coefY % of movement that should be applied
   */
  const parallaxTransformStyle = (
    amplitude: number,
    coefX: number,
    coefY: number,
    scale = 1
  ) => {
    const transformX = amplitude * coefX;
    const transformY = amplitude * coefY;
    return {
      transform: `scale(${scale}) translate(${transformX}px, ${transformY}px)`,
    };
  };

  let previousGamma: number;
  let previousBeta: number;

  const handleOrientation = throttle<DeviceOrientationEvent>((e) => {
    const { gamma, beta } = e as DeviceOrientationEvent;
    if (!gamma || !beta) return;

    // since gamma angle varies between 90 and -90 continuously (90 and -89 is actually the same orientation at almost 1 deg)
    // multiply this by 2 (will vary between 180 and -180)
    // sin 180 and sin -180 have same value so it wont jitter
    const measuredGammaDeg = Math.round(gamma) * 2;
    const measuredBetaDeg = Math.round(beta);

    const measuredGammaRad = (measuredGammaDeg * 2 * Math.PI) / 360;
    const measuredBetaRad = (measuredBetaDeg * 2 * Math.PI) / 360;

    if (previousGamma == undefined) previousGamma = Math.sin(measuredGammaRad);
    if (previousBeta == undefined) previousBeta = Math.sin(measuredBetaRad);

    const newSinGamma = Math.sin(measuredGammaRad);
    const newSinBeta = Math.sin(measuredBetaRad);

    const dSinGamma = newSinGamma - previousGamma;
    const dSinBeta = newSinBeta - previousBeta;

    setCoefX(previousGamma + dSinGamma);
    setCoefY(previousBeta + dSinBeta);

    previousGamma = Math.sin(measuredGammaRad);
    previousBeta = Math.sin(measuredBetaRad);
  }, 1000 / 30);
  useEffect(() => {
    window.addEventListener('deviceorientation', handleOrientation, true);

    return () => {
      window.removeEventListener('deviceorientation', handleOrientation);
    };
  }, []);

  return (
    <div
      onMouseMove={onMouseMove}
      className="bg-red-100 flex-grow relative flex w-full overflow-hidden select-none"
    >
      <Head>
        <title>Yufimia - Home</title>
      </Head>
      <Image
        src={bgImg}
        placeholder="blur"
        layout="fill"
        objectFit="cover"
        objectPosition="center left"
        className="transform transition-transform duration-1000 ease-out"
        style={{
          ...parallaxTransformStyle(-40, coefX, coefY, 1.2),
          pointerEvents: 'none',
        }}
        alt=""
      />
      <Image
        src={farImg}
        placeholder="blur"
        layout="fill"
        objectFit="cover"
        objectPosition="center left"
        className="transition-transform duration-1000 ease-out"
        style={{
          ...parallaxTransformStyle(-100, coefX, coefY),
          pointerEvents: 'none',
        }}
        alt=""
      />
      <div className="absolute top-0 bottom-0 left-0 right-0 w-full md:w-2/3 m-auto">
        <Image
          src={charImg2}
          placeholder="blur"
          layout="fill"
          objectFit="cover"
          objectPosition="center 50px"
          className="transition-transform duration-1000 ease-out transform overflow-visible"
          style={{
            ...parallaxTransformStyle(-20, coefX, coefY, 1.1),
            pointerEvents: 'none',
          }}
          alt="Portrait illustration of Yume Tsuyuno"
        />
      </div>
      <Image
        src={midImg}
        placeholder="blur"
        layout="fill"
        objectFit="cover"
        objectPosition="center left"
        className="transition-transform duration-1000 ease-out"
        style={{
          ...parallaxTransformStyle(50, coefX, coefY),
          pointerEvents: 'none',
        }}
        alt=""
      />
      <Image
        src={ratherCloseImg}
        placeholder="blur"
        layout="fill"
        objectFit="cover"
        objectPosition="center left"
        className="transition-transform duration-1000 ease-out"
        style={{
          ...parallaxTransformStyle(100, coefX, coefY),
          pointerEvents: 'none',
        }}
        alt=""
      />
      <Image
        src={closeImg}
        placeholder="blur"
        layout="fill"
        objectFit="cover"
        objectPosition="center right"
        className="transition-transform duration-1000 ease-out"
        style={{
          ...parallaxTransformStyle(150, coefX, coefY),
          pointerEvents: 'none',
        }}
        alt=""
      />
      <div
        className="transition-transform duration-1000 ease-out"
        style={parallaxTransformStyle(250, coefX, coefY)}
      >
        <Image
          src={veryCloseImg}
          placeholder="blur"
          objectFit="contain"
          width="250%"
          alt=""
        />
      </div>
    </div>
  );
};

export interface HomeBannerProps {}

function throttle<T>(callback: (e: T) => void, wait: number) {
  var timeout: NodeJS.Timeout | undefined;
  return function (e: T) {
    if (timeout) return;
    timeout = setTimeout(() => (callback(e), (timeout = undefined)), wait);
  };
}
