import tiktokIcon from '../public/sns/tiktok.svg';
import twitterIcon from '../public/sns/twitter.svg';
import twitchIcon from '../public/sns/twitch.svg';
import pixivIcon from '../public/sns/pixiv.svg';
import youtubeIcon from '../public/sns/youtube.svg';
import instagramIcon from '../public/sns/instagram.svg';

const socialItems = [
  {
    title: 'twitter',
    iconSrc: twitterIcon,
    url: 'https://twitter.com/tsuyuno_yume',
  },
  {
    title: 'youtube',
    iconSrc: youtubeIcon,
    url: 'https://www.youtube.com/channel/UCU2Qdgx8-48JV8iQr8_Wehg',
  },
  {
    title: 'twitch',
    iconSrc: twitchIcon,
    url: 'https://www.twitch.tv/tsuyuno_yume',
  },
  {
    title: 'tiktok',
    iconSrc: tiktokIcon,
    url: 'https://www.tiktok.com/@tsuyuno_yume',
  },
  {
    title: 'pixiv',
    iconSrc: pixivIcon,
    url: 'https://www.pixiv.net/en/users/59737248',
  },
  {
    title: 'instagram',
    iconSrc: instagramIcon,
    url: 'https://www.instagram.com/tsuyuno_yume/',
  },
];
export default socialItems;
