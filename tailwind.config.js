/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors');
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    colors: {
      ...colors,
      'yume-dark': '#785776',
      'yume-main-1': '#F6D8DF',
      'yume-main-2': '#DC8BAA',
      'yume-secondary': '#FEE9BA',
    },
    extend: {
      aspectRatio: {
        landscape: '16/9',
        '4/3': '4/3',
        '3/4': '3/4',
        portrait: '9/16',
      },
    },
  },
  plugins: [],
};
